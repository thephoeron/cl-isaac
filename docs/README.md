# Documentation

Optimized Common Lisp implementation of Bob Jenkins' ISAAC-32 and ISAAC-64 algorithms, for fast cryptographic random number generation using Indirection, Shift, Accumulate, Add, and Count operators.

---

Copyright &copy; 2022, "the Phoeron" Colin J.E. Lupton.

Back to [//thephoeron.github.io](https://thephoeron.github.io)
